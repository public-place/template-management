var express = require('express')
const mongoose = require('mongoose');
const { Validator } = require('node-input-validator');
const axios = require('axios')

const Category = require('../Scheme/Category');

var router = express.Router()

/**
 * Create new category as a top one or sub one.
 */
 router.post('/:id?', async (req, res) => {
    const v = new Validator(req.body, {
        "displayName": "required"
    });

    const matched = await v.check();

    if(!matched) {
        res.status(422).send(v.errors);
        return 
    }

    // check if parent category is exist
    if(req.params.id) {
        const parentCategory = Category.findById(req.params.id).catch(err => (null))
        if(!parentCategory){
            res.status(409).send("Parent category is not exist");
            return 
        }
    }

    const category = new Category({
        displayName: req.body.displayName,
        categoryId: mongoose.Types.ObjectId(req.params.id)
    })

    category.save(err => {
        if(err) {
            res.status(409).send(err)
        } else {
            res.send(category)
        }
    });
})

/**
 * Get all categories
 */
router.get('', async(req, res) => {
    const categories = await Category.find();

    res.send(categories);
})

/**
 * Get a category by id
 */
router.get('/:id', async(req, res) => {
    const category = await Category.findById(req.params.id)
        .catch(err => {
            return null;
        })

    if(category) {
        res.send(category)
    } else {
        res.sendStatus(404);
    }
})

/**
 * Delete a category.
 * If category has sub categories or templates, then need to delete them as well.
 */
router.delete('/:id', async(req, res) => {

    const categoryId = req.params.id

    Category.findByIdAndRemove(categoryId)
        .then(async (category) => {
            // if category was exist
            if(category) {

                // delete all sub categories
                await Category.deleteMany({
                    categoryId: mongoose.Types.ObjectId(categoryId)
                })

                // delete all sub templates.
                await axios.delete(process.env.TEMPLATE_SERVICE_URL + '/by-category/' + categoryId)

                res.send(category)
            }
            // else
            res.sendStatus(404)
        })
        .catch(err => {
            res.sendStatus(404);
        })
})

/**
 * Move a category to other category's sub one.
 */
router.post('/move/:id', async (req, res) => {
    const v = new Validator(req.body, {
        "categoryId": "required"
    });

    const matched = await v.check();

    if(!matched) {
        res.status(422).send(v.errors);
        return 
    }

    // check if new parent category is exist
    const newParentCategory = await Category.findById(req.body.categoryId)
        .catch((err) => (null))

    if(!newParentCategory) {
        res.status(409).send("New parent category is not exist")
        return;
    }

    // move
    Category.findByIdAndUpdate(req.params.id, {
        categoryId: mongoose.Types.ObjectId(req.body.categoryId)
    })
        .then(category => {
            if(category) {
                res.send(category)
            } else {
                res.sendStatus(404)
            }
        })
        .catch(err => {
            res.sendStatus(404)
        })
})

module.exports = router