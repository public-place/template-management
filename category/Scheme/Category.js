const mongoose = require('mongoose');

const { Schema } = mongoose;

const CategoryScheme = mongoose.model("Category", new Schema({
    displayName: {
        type: String,
        required: true
    },
    categoryId: {
        type: mongoose.Types.ObjectId,
        required: false
    }
}));

module.exports = CategoryScheme;