const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');

const CategoryController = require('./Controller/CategoryController')

// setup environment
require('dotenv').config({
    path: '../.env'
})

const app = express();

app.use(bodyParser.json())

mongoose.connect(process.env.MONGODB_CONNECT_URL,
    (err) => {
        if (err) {
            throw err
        } else {
            console.log('DB is connected')
        }
    }    
)

app.use('/category', CategoryController)

app.listen(3000, () => {
    console.log('Server is running -- Category Service')
})