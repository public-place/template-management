const { should, expect } = require('chai')
const chai = require('chai')
const chaiHttp = require('chai-http')

chai.use(chaiHttp)

// setup environment
require('dotenv').config({
    path: './.env'
})

const templateServiceUrl = process.env.TEMPLATE_SERVICE_URL
const categoryServiceUrl = process.env.CATEGORY_SERVICE_URL

let travelDestinationsCategoryId = '';

describe('1. Insert the category ‘Travel Destinations’', () => {
    it("should create new category", (done) => {
        chai.request(categoryServiceUrl)
            .post("")
            .send({
                displayName: "Travel Destinations"
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                travelDestinationsCategoryId = response.body._id

                done()
            })
    })
})

let mexicoCategoryId = '';

describe("2. Insert the category ‘Mexico’ grouped under ‘Travel Destinations’.", () => {
    it("should create new category under ‘Travel Destinations’", (done) => {
        chai.request(categoryServiceUrl)
            .post("")
            .send({
                displayName: "Mexico",
                categoryId: travelDestinationsCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                mexicoCategoryId = response.body._id

                done()
            })
    })
})

let germanyCategoryId = '';

describe("3. Insert the category ‘Germany’ grouped under ‘Travel Destinations’.", () => {
    it("should create new category under ‘Travel Destinations’", (done) => {
        chai.request(categoryServiceUrl)
            .post("")
            .send({
                displayName: "Germany",
                categoryId: travelDestinationsCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                germanyCategoryId = response.body._id

                done()
            })
    })
})

let acapulcoTemplateId = '';
describe("4. Insert the template ‘acapulco’ grouped under ‘Mexico’.", () => {
    it("should create new template under ‘Mexico’", (done) => {
        chai.request(templateServiceUrl)
            .post("")
            .send({
                displayName: "acapulco",
                categoryId: mexicoCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                acapulcoTemplateId = response.body._id

                done()
            })
    })
})

describe("5. Insert the template ‘munich’ grouped under ‘Germany’", () => {
    it("should create new template under ‘Mexico’", (done) => {
        chai.request(templateServiceUrl)
            .post("")
            .send({
                displayName: "munich",
                categoryId: germanyCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                done()
            })
    })
})

let beachCategoryId = ''

describe("6. Insert the category ‘Beach’ grouped under ‘Germany’ ", () => {
    it("should create new category under ‘Travel Destinations’", (done) => {
        chai.request(categoryServiceUrl)
            .post("")
            .send({
                displayName: "Beach",
                categoryId: germanyCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                beachCategoryId = response.body._id

                done()
            })
    })
})

describe("7. Insert the template ‘los cabos’ grouped under ‘Beach’.", () => {
    it("should create new template under ‘Mexico’", (done) => {
        chai.request(templateServiceUrl)
            .post("")
            .send({
                displayName: "los cabos",
                categoryId: beachCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                done()
            })
    })
})

let exclusiveCategoryId = '';

describe("8. Insert the category ‘Exclusive’ grouped under ‘Beach’", () => {
    it("should create new template under ‘Mexico’", (done) => {
        chai.request(categoryServiceUrl)
            .post("")
            .send({
                displayName: "Exclusive",
                categoryId: beachCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                exclusiveCategoryId = response.body._id

                done()
            })
    })
})

describe("9. Insert the template ‘la paz’ grouped under ‘Exclusive’.", () => {
    it("should create new template under ‘Mexico’", (done) => {
        chai.request(templateServiceUrl)
            .post("")
            .send({
                displayName: "la paz",
                categoryId: exclusiveCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                done()
            })
    })
})

describe("10. Move the category ‘Beach’ to the template ‘acapulco’ (this should fail and nothing should be altered in the db as a consequence)", () => {
    it("this should fail and nothing should be altered in the db as a consequence", done => {
        chai.request(categoryServiceUrl)
            .post("/move/" + beachCategoryId)
            .send({
                categoryId: acapulcoTemplateId
            })
            .end(function(error, response) {
                expect(response).not.have.status(200)
                done()
            })
    })
})

describe("11. Move the category ‘Beach’ to the category ‘Mexico’ ", () => {
    it("should move to Mexico", done => {
        chai.request(categoryServiceUrl)
            .post("/move/" + beachCategoryId)
            .send({
                categoryId: mexicoCategoryId
            })
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                done()
            })
    })
})

describe("12. Delete the category ‘Beach’", () => {
    it("should delete the category ‘Beach’", done => {
        chai.request(categoryServiceUrl)
            .delete("/" + beachCategoryId)
            .end(function(error, response) {
                expect(response).have.status(200)
                expect(response.body).to.be.a('object')

                done()
            })
    })
})

describe("13. Insert the template ‘memo’ which is not grouped under no category", () => {
    it("should not create new template without category", (done) => {
        chai.request(templateServiceUrl)
            .post("")
            .send({
                displayName: "memo",
                categoryId: null
            })
            .end(function(error, response) {
                expect(response).not.have.status(200)

                done()
            })
    })
})