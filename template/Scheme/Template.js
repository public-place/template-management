const mongoose = require('mongoose')

const { Schema } = mongoose;

const Template = mongoose.model("Tempalte", new Schema({
    displayName: {
        type: String,
        required: true
    },
    categoryId: {
        type: mongoose.Types.ObjectId,
        required: true
    }
}));

module.exports = Template;