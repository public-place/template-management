const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const TemplateController = require('./Controller/TemplateController')

// setup environment
require('dotenv').config({
    path: '../.env'
})

const app = express()

// Setup middlewares
app.use(bodyParser.json())

mongoose.connect(process.env.MONGODB_CONNECT_URL,
    err => {
        if(err) {
            throw err;
        } else {
            console.log('DB is connected')
        }
    }
)

app.use('/template', TemplateController)

app.listen(3001, () => {
    console.log('Server is running -- Template Service')
})