var express = require('express')
const mongoose = require('mongoose');
const { Validator } = require('node-input-validator');
const axios = require('axios')

const Template = require('../Scheme/Template');

var router = express.Router()

/**
 * Create new template
 */
 router.post('', async (req, res) => {
    const v = new Validator(req.body, {
        displayName: "required",
        categoryId: "required"
    })

    const matched = await v.check()

    if(!matched) {
        res.status(422).send(v.errors)
        return
    }

    // check if category is exist
    const category = await axios.get(process.env.CATEGORY_SERVICE_URL + '/' + req.body.categoryId)
        .then(response => (response.data))
        .catch(err => null)

    if(!category) {
        res.status(409).send("Category is not found")
        return;
    }

    const template = new Template({
        displayName: req.body.displayName,
        categoryId: mongoose.Types.ObjectId(req.body.categoryId)
    })

    template.save()
        .then(template => {
            res.send(template)
        })
        .catch(err => {
            res.status(409).send(err)
        })
})

/**
 * Get all templates
 */
router.get('', async (req, res) => {
    const templates = await Template.find();

    res.send(templates)
})

/**
 * Get template by id
 */
router.get('/:id', async(req, res) => {
    const template = await Template.findById(req.params.id)
        .catch(err => (null))

    if(template) {
        res.send(template)
    } else {
        res.sendStatus(404)
    }
})

/**
 * Delete template by id
 */
router.delete('/:id', async(req, res) => {
    const template = await Template.findByIdAndRemove(req.params.id)
        .catch(err => (null))

    if(template) {
        res.send(template)
    } else {
        res.sendStatus(404)
    }
})

/**
 * Delete templates which are belonged to specific category
 */
router.delete('/by-category/:categoryId', async(req, res) => {
    Template.remove({
        categoryId: mongoose.Types.ObjectId(req.params.categoryId)
    })
        .then(result => {
            res.send(result)
        })
        .catch(err => {
            res.status(409).send(err)
        })
})

/**
 * Move template to under another category
 */
router.post('/move/:id', async (req, res) => {
    const v = new Validator(req.body, {
        categoryId: "required"
    });

    const matched = await v.check()

    if(!matched) {
        res.status(422).send(v.errors)
    }

    // check if new category is exist
    const category = await axios.get(process.env.CATEGORY_SERVICE_URL + '/' + req.body.categoryId)
        .then(response => (response.data))
        .catch(err => null)

    if(!category) {
        res.status(409).send("Category is not found")
        return;
    }

    // move
    Template.findByIdAndUpdate(req.params.id, {
        categoryId: mongoose.Types.ObjectId(req.body.categoryId)
    })
        .then(template => {
            if(template) {
                res.send(template)
            } else {
                res.sendStatus(404)
            }
        })
        .catch(err => {
            res.sendStatus(404)
        })
})

module.exports = router